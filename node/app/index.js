import express, { query } from 'express'
import mysql from 'mysql'

const app = express()
const port = 3000
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
};


const connection = mysql.createConnection(config)

const sql = `INSERT INTO people(nome) values ('Fernando')`
connection.query(sql)

let texto = '<h1>Code Education Rocks!!</h1>';

connection.query('select * from people', (err, rows) => {
    if(err) throw err;

    rows.forEach(element => {
        texto += '<h3>Id: ' + element.id + ' | Nome: ' + element.nome + '</h3>';
    });
})

connection.end()



app.get('/', (req, res) => {
    res.send(texto);
})






app.listen(port, () => {
    console.log('Rodando na porta ' + port)
})